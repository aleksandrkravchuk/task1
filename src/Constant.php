<?php


namespace Sample;


class Constant
{
    const ERROR_DUPLICATE_CODE = 1062;

    const SAMPLE_BOOK_NAME = 'New book';
}